package com.fourcatsdev.aula02.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fourcatsdev.aula02.orm.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
